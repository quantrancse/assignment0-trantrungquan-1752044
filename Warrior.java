public class Warrior {
    private int baseHP;
    private int wp;

    public Warrior(int baseHP, int wp) {
        if (baseHP < 1 || baseHP > 888 || wp < 0 || wp > 3) 
            System.out.println("Invalid Input");
        else {
            this.baseHP = baseHP;
            this.wp = wp;
        }
    }
    
    public int getBaseHP() {
        return baseHP;
    }

    public int getwp() {
        return wp;
    }
    
    public int getRealHP() {
        if (wp == 0)
            return baseHP / 10;
        else 
            return baseHP;
    }
}
